#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from os.path import join, basename, abspath, dirname, exists
import sys
import shutil
import argparse
import logging
from pytest_ngsfixtures.factories import DATADIR
from snakemake.io import expand

logger = logging.getLogger("setup_data")

ROOTDIR = abspath(dirname(__file__))
WORKING_DIRECTORY = abspath(os.curdir)
OUTPUT_DATADIR = join(WORKING_DIRECTORY, "data")
OUTPUT_DATADIR_LIST = expand("{ROOT}/{NUM}", ROOT=OUTPUT_DATADIR,
                             NUM=[1, 2, 3, 4])
EXAMPLESDIR = join(OUTPUT_DATADIR, "example")

bam = join(DATADIR, "applications", "pe", "medium.bam")

# Dataset 3
samples = ['PUR.HG00731.A', 'PUR.HG00731.B', 'PUR.HG00733.A']
refdata = ['scaffolds.fa', 'scaffolds.dict', 'scaffolds.bed',
           'scaffolds.interval_list']

TINY = join(DATADIR, "tiny")
REFDIR = join(DATADIR, "ref")

sampledata = expand("{TINY}/{SM}{READ}.fastq.gz", SM=samples,
                    READ=["_1", "_2"], TINY=TINY)
refdata = expand("{REFDIR}/{FN}", REFDIR=REFDIR, FN=refdata)
config = join(ROOTDIR, "config.yaml")
config_example = join(ROOTDIR, "config_example.yaml")
data_4 = sampledata + refdata


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Setup test data for snakemake workshop")
    parser.add_argument(
        '-e', '--example', action='store_true',
        help="also create example directory; for snakemake presentation",
        dest="example")
    args = parser.parse_args()

    # Create base directory called data
    if exists(OUTPUT_DATADIR):
        logger.error("Output directory {} exists; aborting".format(OUTPUT_DATADIR))
        sys.exit(1)

    for d in OUTPUT_DATADIR_LIST:
        logger.info("Creating directory {}".format(d))
        os.makedirs(d)
    if args.example:
        logger.info("Creating directory {}".format(EXAMPLESDIR))
        os.makedirs(EXAMPLESDIR)

    # Copy Makefile and Snakefile_1 to data/1
    shutil.copy(join(ROOTDIR, "Makefile"),
                join(OUTPUT_DATADIR_LIST[0], "Makefile"))
    shutil.copy(join(ROOTDIR, "Snakefile_1"),
                join(OUTPUT_DATADIR_LIST[0], "Snakefile"))
    shutil.copy(bam,
                join(OUTPUT_DATADIR_LIST[0], "file.bam"))

    # Copy Snakefile_2 to data/2
    shutil.copy(join(ROOTDIR, "Snakefile_2"),
                join(OUTPUT_DATADIR_LIST[1], "Snakefile.template"))

    # Copy Snakefile_3 to data/3
    shutil.copy(join(ROOTDIR, "Snakefile_3"),
                join(OUTPUT_DATADIR_LIST[2], "Snakefile.template"))

    # Copy sequence and reference data to data/4
    for f in data_4 + [config]:
        shutil.copy(f,
                    join(OUTPUT_DATADIR_LIST[3],
                         basename(f)))
    shutil.copy(join(ROOTDIR, "Snakefile_4"),
                join(OUTPUT_DATADIR_LIST[3], "Snakefile.template"))

    if args.example:
        # Copy sequence and reference data to data/example
        for f in data_4:
            shutil.copy(f,
                        join(EXAMPLESDIR, basename(f)))
        shutil.copy(join(ROOTDIR, "Snakefile_example"),
                    join(EXAMPLESDIR, "Snakefile"))
        shutil.copy(join(ROOTDIR, "bwa.yaml"),
                    join(EXAMPLESDIR, "bwa.yaml"))
        shutil.copy(config_example,
                    join(EXAMPLESDIR, "config.yaml"))
