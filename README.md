## About ##

Example data and presentation material for Snakemake workshop.

## Cloning ##

Clone the repository to a directory of choice, e.g.:

	mkdir -p $HOME/workshop/snakemake && cd $HOME/workshop/snakemake
	git clone git@bitbucket.org:scilifelab-lts/snakemake-workshop.git

All workshop exercises will use paths relative to this directory.

## Software installation ##

### Installing miniconda ###

If you have miniconda3 installed, skip to the next section. If you
haven't already got a miniconda installation, read through the
commands in `scripts/setup.sh` to make sure you understand what it
does, then run the script:

	./scripts/setup.sh

### Creating/updating a miniconda environment ###

If you have miniconda3 installed, create the environment
`snakemake-workshop` with the following command:

	conda create -n snakemake-workshop python=3

and add required dependencies:

	source activate snakemake-workshop
	conda install -c bioconda snakemake bwa picard gatk samtools

Please note that the gatk binary needs to be installed manually.
Download the tar.bz2 file from
https://software.broadinstitute.org/gatk/download/ and run command

	gatk-register /path/to/GenomeAnalysisTK-3.8.0.tar.bz2
