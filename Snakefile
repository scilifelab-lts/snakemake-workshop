import os

rule all:
    """Generate all documents"""
    input: ['rr_and_workflows.revealjs.html', 'snakemake.html']


rule clean:
    """Remove unwanted targets"""
    shell:
        'rm -f *.html *.md *.Rmd; '
        'rm -rf *_files '
           

rule org_to_html:
    """Convert org document to html with ox-reveal.

    This rule requires emacs>=25, org-mode>=9.0 and the custom package
    org-reveal from https://github.com/yjwen/org-reveal
    """
    params:
        customlisp = os.path.expandvars('$HOME/.emacs.d/elisp'),
        elpalisp = os.path.expandvars('$HOME/.emacs.d/elpa'),
    input: org = "{prefix}.org"
    output: html = "{prefix}.html"
    shell:
        'emacs --batch -Q --debug-init '
        '--eval "(add-to-list \'load-path \\"{params.customlisp}\\" )" '
        '--eval "(let ((default-directory  \\"{params.elpalisp}\\"))  (setq load-path (append (let ((load-path  (copy-sequence load-path))) (normal-top-level-add-subdirs-to-load-path)) load-path)))" '
        '--eval "(require \'org)" '
        '--eval "(message \\"org-version: %s\\" (org-version))" '
        '--eval "(require \'org-ref)" '
        '--eval "(require \'ox-reveal)" '
        '--eval "(custom-set-variables \'(make-backup-files nil))" '
        '--visit "{input.org}" '
        '--funcall org-reveal-export-to-html '


rule org_to_rmd:
    """Convert org document to Rmd.

    This rule requires emacs>=25, org-mode>=9.0 and the custom package
    ox-ravel from https://github.com/chasberry/orgmode-accessories

    NB: currently this command throws an error:

    'Wrong type argument: arrayp, nil'

    which is thrown by the org-ravel-export-to-file statement.

    Apparently some configuration setting is missing; if the command
    is run without the batch flag it works.

    """
    params:
        customlisp = os.path.expandvars('$HOME/.emacs.d/elisp'),
        elpalisp = os.path.expandvars('$HOME/.emacs.d/elpa'),
    input: org = "{prefix}.org.bak"
    output: rmd = "{prefix}.Rmd"
    shell:
        'emacs --batch -Q --debug-init '
        '--eval "(add-to-list \'load-path \\"{params.customlisp}\\" )" '
        '--eval "(let ((default-directory  \\"{params.elpalisp}\\"))  (setq load-path (append (let ((load-path  (copy-sequence load-path))) (normal-top-level-add-subdirs-to-load-path)) load-path)))" '
        '--eval "(require \'org)" '
        '--eval "(message \\"org-version: %s\\" (org-version))" '
        '--eval "(require \'org-ref)" '
        '--eval "(require \'ox-ravel)" '
        '--eval "(custom-set-variables \'(make-backup-files nil))" '
        '--visit "{input.org}" '
        '--eval "(org-ravel-export-to-file \'ravel-markdown \\"{output.rmd}\\")" '


rule markdown_to_rmarkdown:
    """Simply hard link markdown file to Rmarkdown.

    rmarkdown::render requires the file suffix to be Rmd for execution
    of R commands. org-md-export-to-markdown only exports to md.
    """
    input: md = "{prefix}.md"
    output: rmd = "{prefix}.Rmd"
    shell:
        'ln {input.md} {output.rmd}'

        
rule rmarkdown_html:
    """Create html file from Rmarkdown."""
    input: rmd = "{prefix}.Rmd",
    output: html = "{prefix}.html"
    shell:
        "Rscript -e 'rmarkdown::render(\"{wildcards.prefix}.Rmd\",\
        output_file=\"{wildcards.prefix}.html\")'"
        

ruleorder: rmarkdown_html > org_to_html > org_to_rmd > markdown_to_rmarkdown 
