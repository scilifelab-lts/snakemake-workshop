#! /bin/bash
#
# Setup miniconda environment for analysis
#

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1} - proceed? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

# Change MINICONDA if you want miniconda installation to reside
# elsewhere than $HOME/miniconda3/
MINICONDA="$HOME/miniconda3/"

# Get miniconda installer
confirm "Download miniconda install script to current directory? : 'wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh'" && wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

# Install miniconda
confirm "Install miniconda at $MINICONDA?: 'bash Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA'" && bash Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA

# Add miniconda to PATH; note that this must be set again whenever you
# launch a new session
export PATH="$MINICONDA/bin:$PATH"

# Create snakemake-workshop environment
confirm "Create snakemake-workshop virtual environment?" && conda create -n snakemake-workshop python=3

# If we didn't want to create environment, exit
if [ $? == 1 ]; then
    echo "Not creating snakemake-workshop environment; quitting"
    exit
fi

# Create a new conda environment called snakemake-workshop, activate
# it and install dependencies
source activate snakemake-workshop

echo "Going to install required dependencies"
echo "Please note that the gatk binary needs to be installed manually"
echo "Download the tar.bz2 file from https://software.broadinstitute.org/gatk/download/"
echo "and run command 'gatk-register /path/to/GenomeAnalysisTK-3.8.0.tar.bz2'"

conda install -c bioconda snakemake bwa picard gatk samtools

